﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Windows;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Interactivity : MonoBehaviour
{

    //three features
    //1. Buttons used to start music and pull up UI Graphic
    //2. Changing the sprite used for a UI Button depending on activation status
    //3. Increasing the volume of a particular voice part when the bio of that singer is being viewed

	//buttons and images
	public Button noteButton; //rest
	public Sprite newsprite; //music note
	public GameObject bioCurrent; //default bio
	public GameObject bioHS; //high school bio
	public GameObject goSwitch;
	public GameObject goReturn;
    public Button buttonSwitch;
	public Button buttonReturn;

	//background
	public GameObject bg2014;
	public GameObject bg2015;
	public GameObject bg2016;
	public GameObject bg2017;
	private float bgCount = 1;
	public GameObject bgInstruction;

	//mixers
	public AudioMixer voicePart; //voice part master

	//volume
	private float initialVol = -80;
	private float bioVol = -5;
	private float backgroundVol = -20;

	//logic
	private bool Activated = false;


	// Start is called before the first frame update
	void Start()
	{
		voicePart.SetFloat("partVol", initialVol);
	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnEnable()
	{
		//Register Button Events
		noteButton.onClick.AddListener(() => buttonCallBack(noteButton)); //note
		buttonReturn.onClick.AddListener(() => buttonCallBack(buttonReturn)); //return
		buttonSwitch.onClick.AddListener(() => buttonCallBack(buttonSwitch)); //switch

	}

	private void buttonCallBack(Button buttonPressed)
	{
		if (buttonPressed == noteButton) //Note / Rest
		{
			//For debugging
			Debug.Log("Clicked: " + noteButton.name);

			noteButton.image.overrideSprite = newsprite; //updates rest to note
			bgInstruction.SetActive(false); //removes instruction text

			Activated = true;
			bioCurrent.SetActive(true);
			goSwitch.SetActive(true);
			goReturn.SetActive(true);
			voicePart.SetFloat("partVol", bioVol);
		}
		else if (buttonPressed == buttonReturn) //Return
		{
			//For debugging
			Debug.Log("Clicked: " + buttonReturn.name);

			if (Activated)
			{
				bioCurrent.SetActive(false);
				bioHS.SetActive(false);
				goSwitch.SetActive(false);
				goReturn.SetActive(false);
				voicePart.SetFloat("partVol", backgroundVol);

				if (bgCount == 1)
				{
					bg2014.SetActive(false);
					bg2015.SetActive(true);
					bg2016.SetActive(false);
					bg2017.SetActive(false);
					bgCount += 1;
				}
                else if (bgCount == 2)
				{
					bg2014.SetActive(false);
					bg2015.SetActive(false);
					bg2016.SetActive(true);
					bg2017.SetActive(false);
					bgCount += 1;
				}
				else if (bgCount == 3)
				{
					bg2014.SetActive(false);
					bg2015.SetActive(false);
					bg2016.SetActive(false);
					bg2017.SetActive(true);
					bgCount += 1;
				}
				else if (bgCount == 4)
				{
					bg2014.SetActive(true);
					bg2015.SetActive(false);
					bg2016.SetActive(false);
					bg2017.SetActive(false);
					bgCount = 1;
				}
			}
		}
		else if (buttonPressed == buttonSwitch) //Switch
		{
			//For debugging
			Debug.Log("Clicked: " + buttonSwitch.name);

			if (bioCurrent.active)
			{
				bioCurrent.SetActive(false);
				bioHS.SetActive(true);
			}
			else if (bioHS.active)
			{
				bioHS.SetActive(false);
				bioCurrent.SetActive(true);
			}
		}
	}
}