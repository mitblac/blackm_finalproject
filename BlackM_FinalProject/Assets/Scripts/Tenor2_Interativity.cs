﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Windows;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Tenor2_Interativity : MonoBehaviour
{

	//buttons and images
	public Button t2Button; //rest
	public Sprite newsprite; //music note
	public GameObject bioCurrent; //default bio
	public GameObject bioHS; //high school bio
	public GameObject goSwitch;
	public GameObject goReturn;
	public Button buttonSwitch;
	public Button buttonReturn;

	//mixers
	public AudioMixer ten2; //tenor2 master

	//volume
	private float initialVol = -80;
	private float bioVol = 0;
	private float backgroundVol = -20;

	//logic
	private bool Activated = false;


	// Start is called before the first frame update
	void Start()
	{
		ten2.SetFloat("t2Vol", initialVol);
	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnEnable()
	{
		//Register Button Events
		t2Button.onClick.AddListener(() => buttonCallBack(t2Button)); //note
		buttonReturn.onClick.AddListener(() => buttonCallBack(buttonReturn)); //return
		buttonSwitch.onClick.AddListener(() => buttonCallBack(buttonSwitch)); //switch

	}

	private void buttonCallBack(Button buttonPressed)
	{
		if (buttonPressed == t2Button) //Note / Rest
		{
			//Your code for button 1
			Debug.Log("Clicked: " + t2Button.name);

			t2Button.image.overrideSprite = newsprite; //updates rest to note

			Activated = true;
            bioCurrent.SetActive(true);
			goSwitch.SetActive(true);
			goReturn.SetActive(true);
			ten2.SetFloat("t2Vol", bioVol);
		}
		else if (buttonPressed == buttonReturn) //Return
		{
			//Your code for button 1
			Debug.Log("Clicked: " + buttonReturn.name);

            if (Activated)
			{
				bioCurrent.SetActive(false);
				bioHS.SetActive(false);
				goSwitch.SetActive(false);
				goReturn.SetActive(false);
				ten2.SetFloat("t2Vol", backgroundVol);
			}
		}
		else if (buttonPressed == buttonSwitch) //Switch
		{
			//Your code for button 1
			Debug.Log("Clicked: " + buttonSwitch.name);

			if (bioCurrent.active)
			{
				bioCurrent.SetActive(false);
				bioHS.SetActive(true);
			}
			else if (bioHS.active)
			{
				bioHS.SetActive(false);
				bioCurrent.SetActive(true);
			}
		}
	}
}